# Glossary

The **Glossary of Confirmed Terms** describes the SensioID Network as it exists today. The **Terms in Development** are work in progress and may be subject to change as the SensioID Protocol is further developed.

## Glossary of Confirmed Terms

### Claim, Verifiable Claim

A **Claim** (or "SensioID Claim") is a collection of data about an Entity on the SensioID Network. SensioID Claims are batched for scalability reasons and anchored to a blockchain, which proves that the Claim existed at the time the block was created. Claims in the SensioID Network ecosystem are Verifiable Claims by default.

### Claim Batching

Claim batching is the process by which many (potentially unlimited) verifiable claims are grouped together into one blockchain transaction. This allows the SensioID network to scale to millions of users while avoiding the transaction limitations of any blockchain.

### Claim Type

A reference to a schema for a particular type of Claim (e.g., Creative Work or Ownership).

### Creative Work

A SensioID Entity representing an original creative work (which could be any digital file such as a text file, a PDF, an image, a video, etc.), created by the Entity registering the Creative Work Claim Type on the SensioID Network.

### Creative Work Claim

A Claim Type describing a Creative Work. It must contain a hash of the work and a digital signature.

### Proof of Existence or Timestamp

Storing the hash of a creative work file on the blockchain so it can be referenced later, proving the existence of that file at a specific point in time. AKKA Timestamping

### Proof of Ownership

Independent verification process using the Sensio Ownership Flow. This created verifiable claim that is signed by both, user and the system (SensioID).

### Sensio Ownership Flow

1. QRcode based verification of the cameras and lenses
2. more to come ...

### Issuer

Crypto person issuer

### Publisher

crypto person publisher, akka system who publishes the claim

### Shares

A list of the shares

### Share

Share is a part or portion of owned/rented thing. In SensioID Network this will be photo equipment or multimedia.

Properties:

- `name`: String - Describes the unit of the share. For now allowed only **percentage**
- `symbol`: String - Short representation of the unit. **%**
- `value`: String - The Value of a unit

```json
{
  "name": "percentage",
  "symbol": "%",
  "value": 100
}
```

### Signature

Digital signature

Properties:

- `fingerprint`: String - PGP Key fingerprint encoded using [Multibase](https://github.com/multiformats/js-multibase) encoding. [Example is here](https://runkit.com/woss/5d11695c293c9e001a370ee1)
- `algorithm`: String - Algorithm used to create a signature
- `signature`: String - Base58btc encoded `multihash(buffer,'sha2-256')`

```json
{
  "fingerprint": "fd1f5e247a976f1e3c14f0a437a6db9962ef3978e",
  "algorithm": "Ed25519Signature2018",
  "signature": "2321edsadjaksgdoasudygasudvasldfasdfasduysaduyas"
}
```
