# Architecture

Sensio Network is a Substrate based blockchain with shared security and builtin Hybrid Consensus.

## General Overview of the process

![Sensio-Network-Statements&Claims](../../assets/Sensio-Network-Statements&Claims.svg)
