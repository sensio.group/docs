# Introduction

> Ego quod meum est habere.
>
> Roughly translates to I own what is mine.
>
> _Unknown_

## Abstract

Blockchain networks opened the possibility of new institutional models built with open source code, able to resist censorship and scale participation globally. However, Bitcoin’s original White Paper description of “one-CPU-one-vote” shaped the industry to think governance centered around machines, not people. Although a fundamental right to privacy bent early blockchain design towards anonymity, the lack of a robust notion of unique identity renders its governance practices into plutocracies: membership is often defined by stake ownership, enabling large holders to swing votes according to their own preferences.

## Incentives

## Conclusions
